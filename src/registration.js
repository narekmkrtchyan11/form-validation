import {
    required,
    handleSubmit,
} from "./utils.js";

import {
    minLength,
    maxLength,
    checkEmail,
    checkPhoneNumber,
} from "./validationHelpers.js";

const form = document.querySelector(".form");
const errorElements = document.querySelectorAll("[data-id-error]")

const formValidationsConfig = {
    firstName: [required, (val) => maxLength(val, 15), (val) => minLength(val, 3)],
    lastName: [required, (val) => maxLength(val, 15), (val) => minLength(val, 3)],
    email: [required,checkEmail],
    password: [required, (val) => maxLength(val, 25), (val) => minLength(val, 6)],
    phoneNumber: [required, (val) => checkPhoneNumber(val)]
}

form.addEventListener("submit", function(e) {
    handleSubmit(e, formValidationsConfig, errorElements);
});




